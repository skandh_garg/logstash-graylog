# README #
LOGSTASH-GRAYLOG SETUP

### What is this repository for? ###

This document explains the steps to install logstash and graylog and also how to send your logs from logstash to graylog.


### How do I get set up? ###

Vagrant File for Graylog and Logstash setup

>>>>>>>>Step 1: Create a VagrantFile and place this in it.



###########################################

Vagrant.configure(2)  do |config|
  config.vm.box = "ubuntu/trusty64"
  config.vm.hostname = "graylog"

  config.vm.network :forwarded_port, guest: 80, host: 8080
  config.vm.network :forwarded_port, guest: 12201, host: 12201, protocol: 'udp'
  config.vm.network :forwarded_port, guest: 12201, host: 12201, protocol: 'tcp'
  config.vm.network "private_network", ip: "192.168.27.11"
  config.vm.provider "virtualbox" do |v|
    v.memory = 3000
  end

  $script = <<SCRIPT
    apt-get update
    apt-get install -y software-properties-common python-software-properties
    echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
    add-apt-repository ppa:webupd8team/java -y
    apt-get -y update
    apt-get install oracle-java8-installer
    apt-get install -y oracle-java8-set-default
    curl -L -O https://artifacts.elastic.co/downloads/logstash/logstash-5.5.1.deb
    sudo dpkg -i logstash-5.5.1.deb
    sudo apt-get install jruby -y
    sudo apt-get install gem -y
    echo 'Going to download Graylog...'
    curl -S -s -L -O https://packages.graylog2.org/releases/graylog-omnibus/ubuntu/graylog_latest.deb
    dpkg -i graylog_latest.deb
    rm graylog_latest.deb
    graylog-ctl set-external-ip http://127.0.0.1:8080/api
    graylog-ctl reconfigure
SCRIPT
  config.vm.provision "shell", inline: $script
end

#########################################


>>>>>>>>Step 2: vagrant up
 
 
>>>>>>>>Step 3: Now you need to install gelf in logstash

For this
Go to :  /usr/share/logstash

then run the command bin/logstash-plugin install logstash-output-gelf

>>>>>>>>Step 4: Change the bind address of elasticsearch to 0.0.0.0

Go to : /opt/graylog/elasticsearch/config

edit elasticsearch.yml and set network.bind_host: 0.0.0.0

>>>>>>>>Step 5: Create your logstash config file

Go to : /usr/share/logstash/

Then create logstash-sample.conf file

  touch logstash-sample.conf
  
>>>>>>>>Step 6: Edit logstash-sample.conf

Add the following lines to it-

#######################
input{
      stdin{}
}
output{
      stdout{}
      gelf{
          host=>"192.168.27.11"
          port=>12202  #It is not 12201        
      }
}
######################

>>>>>>>>Step 7: Open Graylog and create a Gelf-Udp Input

Open browser and go to- http://localhost:8080
Login to graylog using username->admin, password-> admin
Click on System/Input and then Inputs
Select Gelf-UDP and create a new input
Set the bind address to 192.168.27.11 and port to 12202
Hit create

>>>>>>>>Step 8: Run your logstash 

Go to : /usr/share/logstash
Run the command: bin/logstash -f logstash-sample.conf

>>>>>>>>Step 9: Now type something in terminal and see the output in graylog


### Who do I talk to? ###

Skandh Garg